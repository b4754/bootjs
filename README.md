# bootjs

A JavaScript library for increased footprint, easily embedded

![boots](/boots.webp)

## Getting started

Simply import the JavaScript into your application to get going:

```
<script src="boot.js"></script>
```
